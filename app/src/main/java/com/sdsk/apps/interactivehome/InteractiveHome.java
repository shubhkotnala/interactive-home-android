package com.sdsk.apps.interactivehome;

import android.app.Application;

import net.gotev.speech.Logger;
import net.gotev.speech.Speech;

/**
 * Created by shubhkotnala on 14/1/18.
 */

public class InteractiveHome extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Speech.init(this, getPackageName());
        Logger.setLogLevel(Logger.LogLevel.DEBUG);
    }
}
