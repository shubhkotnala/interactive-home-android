package com.sdsk.apps.interactivehome;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URLEncoder;

/**
 * Created by shubhkotnala on 14/1/18.
 */

public class SendToNetwork extends AsyncTask<String,Void, Socket> {

    private Exception exception;
    Socket socket = null;
    DataOutputStream DOS = null;

    @Override
    protected Socket doInBackground(String... params) {

        try{
            socket = new Socket("192.168.18.244",2468);
            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
        //    DOS = new DataOutputStream(socket.getOutputStream());
            String s1 = URLEncoder.encode(params[0],"UTF-8");
            osw.write(s1,0,s1.length());
        //    DOS.writeUTF(s1);
        //    DOS.flush();
        //    DOS.close();
            socket.close();
            Log.d("Socket","Successful "+params[0]);
          //  Toast.makeText(ge,"Data sent successfully",Toast.LENGTH_SHORT).show();
        } catch (IOException e){
            Log.d("Socket","Unsuccessful " + e);
         //   Toast.makeText(this,"Data NOT sent",Toast.LENGTH_SHORT).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Socket socket) {
        super.onPostExecute(socket);
    }
}
